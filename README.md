## Sberbank Russian Housing Market

---
[click here](https://www.kaggle.com/c/sberbank-russian-housing-market/data)

### Чтобы запустить проект необходимо выполнить следующие шаги:

1. убедиться, что вы находитесь в branch `main`, при необходимости переключиться на эту ветку - она основная
2. скачать данные, для этого в папке `data` лежит инструкция __data_download.sh__. Запустите этот файл на своем
   компьютере через терминал

* Возможно, у вас не установлен пакет Python и Kaggle, в этом случае следуйте инструкциям ниже:
  1. Клонируем проект к себе на компьютер (кнопка `clone` в верхнем правом углу)
  2. Создаем Virtual environment
   ```
   $ python3 -m venv env 
   $ source env/bin/activate 
   $ deactivate 
   ```
  3. Устанавливаем необходимые библиотеки, в том числе и kaggle, и фиксируем `requirements.txt`
   ```
   pip install numpy pandas matplotlib seaborn scikit-learn jupyter kaggle 
   pip freeze > requirements.txt
   pip install -r requirements.txt
   ```
  4. Устанавливаем связь с Kaggle <br>
     https://github.com/Kaggle/kaggle-api
   ```
   cp ~/Downloads/kaggle.json ~/.kaggle/kaggle.json
   chmod 600 ~/.kaggle/kaggle.json
   ```

3. Проверяем, что всё установленно верно, смотрим версию питона
   ```
   python3
   where python3
   ```

__в данном проекте:__

- `/usr/local/opt/python@3.8/bin/python3` - local base env
- `/Users/mac/ml_2021/env/bin/python` - our env

* Python 3.8.10 (default, May 4 2021, 03:06:52) - default version of Python in env

4. Основной ноутбук лежит в папке `notebooks` и называется `ml_hw1_Bubnova.ipynb`
   В нем описан процесс загрузки данных, их очистки и создания различных моделей. Также если открыть ноутбук в Jupyter,
   то будет работать оглавление (через браузер Gitlab не работает, файл нужно скачивать).
5. В основной репозиторий также добавлен файл `kaggle_score`, в которым можно изучить качество модели по оценке kaggle (
   всё это также продублировано на графике с результатами в самом ноутбуке)

## Kaggle Score for each model

![](kaggle_score.png)
